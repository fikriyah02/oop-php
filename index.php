<?php
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");

    $binatang = new Animal("shaun");
    echo "Name : " . $binatang->name."<br>";
    echo "Legs : " . $binatang->legs."<br>";
    echo "Cold blooded : " . $binatang->cold_blooded."<br><br>";

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name."<br>";
    echo "Legs : " . $kodok->legs."<br>";
    echo "Cold blooded : " . $kodok->cold_blooded."<br>";
    echo "Jump : " . $kodok->jump."<br><br>";
    
    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong->name."<br>";
    echo "Legs : " . $sungokong->legs."<br>";
    echo "Cold blooded : " . $sungokong->cold_blooded."<br>";
    echo "Yell : " . $sungokong->yell."<br>";
    
    
    
?>